This is a simple HTML5 presentation application. Using this app, clients can synchronise their sildeshow presentation. 
This app is developed using latest HTML5 technologies such as (Websockets). Also the application uses latest CSS3 transformations for styling.

Usage:
In the terminal change the location to the directory in which this project is cloned.
Run npm install.

Test:
Open http://localhost:8000