/* server.js */

var express = require('express');
var app = express();
var http = require('http');


app.set('view engine', 'ejs');


app.use(express.static(__dirname + '/public'));

// routes for app
app.get('/', function(req, res) {
  res.render('pad');
});
app.get('/(:id)', function(req, res) {
  res.render('pad');
});


var server = http.createServer(app);
var WebSocketServer = require('ws').Server;
var wss = new WebSocketServer({
  server: server
});

var clients = [];


//handling websocket messages
wss.on('connection', function connection(ws) {
 
  clients.push(ws);

  ws.on('message', function(message) {
    //broadcast the message to all the clients
   console.log(message);
    clients.forEach(function(client) {
      client.send(message);

    });
  });
});


var port = process.env.PORT || 8000;
server.listen(port);
console.log("Server listening at http://localhost: " + port);
