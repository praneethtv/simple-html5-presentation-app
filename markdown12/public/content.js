﻿
var currentslide = 0;

var autoplay = false;
var playtime = 3000;
var timer;

var looping = true;

var lastid = "";
var player;
var playertime;
var playerstate;
/*var byId = function (id) {
    return $("#id").addClass('active');
};*/

/*  function to change the window hash */
var getelementid = function () {

    return window.location.hash.replace(/^#\/?/, "");
};

var YTdeferred = $.Deferred();

window.onYouTubeIframeAPIReady = function () {
    console.log("!!!!!!");
    YTdeferred.resolve(window.YT);
};


var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);


$(function () {

    var change = false;
    var playertime;
    var playerstate;

  //  console.log(window.location.host + document.location.pathname);
    var ws = new WebSocket('ws://' + window.location.host);

    ws.onopen = function(e) {
      console.log("Connection established!");
      ws.send('Hello Me!');

//function that sends message to the server on hashchange
      $(window).on('hashchange', function () {

          if (change) {
            return;
          }
          var hash = window.location.hash;

          ws.send(JSON.stringify({
            msg:'slidechange',
            hash:hash
          }));

        });
//youtube player initialisation
        YTdeferred.done(function (YT) {

            player = new YT.Player('myYTPlayer', {

                videoId: '7V-fIGMDsmE',
                events: {
                 // 'onReady': onPlayerReady,
                    'onStateChange': onPlayerStateChange
                  }
                });

              });

      function onPlayerStateChange(event) {

        var newState = event.data;
        playertime = player.getCurrentTime();
        playerstate = player.getPlayerState();
        console.log(playerstate);

          ws.send(JSON.stringify({
                  msg : 'setplayerstatus',
                  playertime: playertime,
                  playerstate: playerstate
              }));
    }
};

//handling messages recevied from server through websockets
  ws.onmessage = function (event) {
    var message = JSON.parse(event.data)
    if(message.msg === "slidechange"){
        window.location.hash = message.hash;

          change = true;

          setInterval(function () {
              change = false;
          }, 100);
    }
    else if (message.msg === "setplayerstatus"){
      console.log("testing");
      playertime = message.playertime;
      playerstate = message.playerstate;

      if (playerstate === 1 || playerstate ===3) {
          player.seekTo(playertime);
          player.playVideo();
      }
      else if (playerstate === 2) {
          player.pauseVideo();
      }
      else {
         player.stopVideo();
      }

    }
};

//handling the user inputs
    $(document).keyup(function (e) {

        if (e.keyCode === 39 || e.keyCode === 40 || e.keyCode === 32) {
            next();
        }
        else if (e.keyCode === 37 || e.keyCode === 38) {

            back();
        }
        else if (e.keyCode === 87) {

            $('.slides').fadeToggle();
        }

    });
    $('.slides').swipe({
        swipe: function (event, direction, distance, duration, fingerCount) {
            switch (direction) {
                case "left":

                    next();
                    break;
                case "right":

                    back();
                    break;

            }
        }
    });

    /*$(document).dblclick(function () {
        window.fullScreenApi.requestFullScreen(document.body);
    });*/

    $(document).keyup(function (e) {
        if (e.keyCode === 70) {
            window.fullScreenApi.requestFullScreen(document.body);
        }

    });

    initialize();
});




function initialize() {
    goto(0);
   // $('section').eq(currentslide).addClass('active');
}

//function that handles the forward slide changes
function next() {
    goto(currentslide + 1);

}

//function that handles the backward slide changes
function back() {
    goto(currentslide - 1);
}

//function that handles the visibility of the current slide
function goto(n) {
    if (autoplay) clearTimeout(timer);
    if (looping) {
        if (n < 0) {
            currentslide = $('section').length - 1;
            window.location.hash = lastid = "#" + $('section').eq(currentslide).attr('id');
        }
        else if (n > $('section').length - 1) {
            currentslide = 0;
            window.location.hash = lastid = "#" + $('section').eq(currentslide).attr('id');
        }
        else currentslide = n; window.location.hash = lastid = "#" + $('section').eq(currentslide).attr('id');
    }

    else {
        if (n > -1 && n < $('section').length) {
            currentslide = n;
            window.location.hash = lastid = "#" + $('section').eq(currentslide).attr('id');
        }
        else {
            return;
        }
    }

    $('section').removeClass('active').eq(currentslide).addClass('active');

    if (autoplay) {
        changeslide();
    }

}


function changeslide() {
    var timeduration = playtime;
    timer = setTimeout(function () { next(); }, timeduration);
}


window.addEventListener("hashchange", function () {

    if (window.location.hash !== lastid) {
        goto(getelementid());
    }
}, false);



// full screen api from John Dyer's code
(function () {
    var
        fullScreenApi = {
        supportsFullScreen: false,
        isFullScreen: function () { return false; },
        requestFullScreen: function () { },
        cancelFullScreen: function () { },
        fullScreenEventName: '',
        prefix: ''
    },
        browserPrefixes = 'webkit moz o ms khtml'.split(' ');

    // check for native support
    if (typeof document.cancelFullScreen != 'undefined') {
        fullScreenApi.supportsFullScreen = true;
    } else {
        // check for fullscreen support by vendor prefix
        for (var i = 0, il = browserPrefixes.length; i < il; i++) {
            fullScreenApi.prefix = browserPrefixes[i];

            if (typeof document[fullScreenApi.prefix + 'CancelFullScreen' ] != 'undefined') {
                fullScreenApi.supportsFullScreen = true;

                break;
            }
        }
    }

    // update methods to do something useful
    if (fullScreenApi.supportsFullScreen) {
        fullScreenApi.fullScreenEventName = fullScreenApi.prefix + 'fullscreenchange';

        fullScreenApi.isFullScreen = function () {
            switch (this.prefix) {
                case '':
                    return document.fullScreen;
                case 'webkit':
                    return document.webkitIsFullScreen;
                default:
                    return document[this.prefix + 'FullScreen'];
            }
        }
        fullScreenApi.requestFullScreen = function (el) {
            return (this.prefix === '') ? el.requestFullScreen() : el[this.prefix + 'RequestFullScreen']();
        }
        fullScreenApi.cancelFullScreen = function (el) {
            return (this.prefix === '') ? document.cancelFullScreen() : document[this.prefix + 'CancelFullScreen']();
        }
    }

    // jQuery plugin
    if (typeof jQuery != 'undefined') {
        jQuery.fn.requestFullScreen = function () {

            return this.each(function () {
                if (fullScreenApi.supportsFullScreen) {
                    fullScreenApi.requestFullScreen(this);
                }
            });
        };
    }

    // export api
    window.fullScreenApi = fullScreenApi;
})();
